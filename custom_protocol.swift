protocol Student {
    var name: String { get }
    var grade: Int? { get }
    
    func sayHello()
}

class LETIStudent: Student {
    var name: String
    var grade: Int?
    init(name: String) {
        self.name = name
    }
    
    func sayHello() {
        print("Привет, я студент ЛЭТИ! Меня зовут \(name).")
    }
}

let student1 = LETIStudent(name: "Амир")
student1.sayHello()