protocol GameDice {
    var numberDice: String { get }
}

extension Int: GameDice {
    var numberDice: String {
        return "Выпало \(self) на кубике"
    }
}
let diceCoub = 4
print(diceCoub.numberDice)