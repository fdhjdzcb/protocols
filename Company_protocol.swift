protocol WriteCode {
    var time: String { get set }
    var countCode: Int { get set }
    
    func writeCode()
}

protocol StopCoding {
    func stopCoding()
}

class Company: WriteCode, StopCoding {
    var time: String
    var countCode: Int
    
    var countDevelopers: Int
    var developersSpecialization: [String] = []
    
    func addNewDev(devSpecialization: String) {
        countDevelopers += 1
        developersSpecialization.append(devSpecialization)
    }
    
    init(time: String, countCode: Int, countDevelopers: Int) {
        self.time = time
        self.countCode = countCode
        self.countDevelopers = countDevelopers
    }
    
    func writeCode() {
        print("Разработка начата")
    }
    
    func stopCoding() {
        print("Разработка остановлена")
    }
}

let company = Company(time: "2 недели", countCode: 12345, countDevelopers: 0)
company.writeCode()
company.addNewDev(devSpecialization: "Android")

print(company.countDevelopers)
print(company.developersSpecialization)

company.stopCoding()