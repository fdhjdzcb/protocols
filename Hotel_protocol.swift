protocol Hotel {
    init (roomCount: Int)
}

class HotelAlfa: Hotel {
    var roomCount: Int = 0
    
    required init(roomCount: Int) {
        self.roomCount = roomCount
    }
}
var hotel = HotelAlfa(roomCount: 100)